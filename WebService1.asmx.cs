﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace JqueryAjaxTest
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        //[WebMethod]
        //public List<PurchaseOrderDTO> GetData(string suppRef, int pageNumber, int pageSize)
        //{
        //    if (suppRef.Trim() == string.Empty)
        //    {
        //        suppRef = null;
        //    }
        //    WideWorldImportersEntities context = new WideWorldImportersEntities();
        //    var list = context.PurchaseOrders
        //        .Where(x => x.SupplierReference
        //        .Contains(suppRef.Trim()) || suppRef == null)
        //        .OrderBy(o => o.SupplierID)
        //        .Skip((pageNumber - 1) * pageSize)
        //        .Take(pageSize)
        //        .ToList()
        //        .Select(t => new PurchaseOrderDTO
        //        {
        //            SupplierReference = string.Concat(t.SupplierReference, "-", t.PurchaseOrderID),
        //            SupplierID = t.SupplierID
        //        }).ToList();

        //    return list;
        //}

        [WebMethod]
        public List<PurchaseOrderDTO> GetData(string suppRef, int pageNumber, int pageSize)
        {
            if (suppRef.Trim() == string.Empty)
            {
                suppRef = null;
            }
            WideWorldImportersEntities context = new WideWorldImportersEntities();
            var list = context.PurchaseOrders
                .Where(x => x.SupplierReference
                .Contains(suppRef.Trim()) || suppRef == null)
                .OrderBy(o => o.SupplierID)             
                .ToList()
                .Select(t => new PurchaseOrderDTO
                {
                    SupplierReference = string.Concat(t.SupplierReference, "-", t.PurchaseOrderID),
                    SupplierID = t.SupplierID
                }).ToList();

            return list;
        }

        public class PurchaseOrderDTO
        {
            public int PurchaseOrderID { get; set; }
            public int SupplierID { get; set; }
            public System.DateTime OrderDate { get; set; }
            public int DeliveryMethodID { get; set; }
            public int ContactPersonID { get; set; }
            public Nullable<System.DateTime> ExpectedDeliveryDate { get; set; }
            public string SupplierReference { get; set; }
            public bool IsOrderFinalized { get; set; }
            public string Comments { get; set; }
            public string InternalComments { get; set; }
            public int LastEditedBy { get; set; }
            public System.DateTime LastEditedWhen { get; set; }
            //public virtual DeliveryMethod DeliveryMethod { get; set; }
            //public virtual Person Person { get; set; }
            //public virtual Person Person1 { get; set; }
            //public virtual ICollection<PurchaseOrderLine> PurchaseOrderLines { get; set; }
            //public virtual Supplier Supplier { get; set; }
            //public virtual ICollection<SupplierTransaction> SupplierTransactions { get; set; }
            //public virtual ICollection<StockItemTransaction> StockItemTransactions { get; set; }
        }
    }
}
