﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="JqueryAjaxTest._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <html xmlns="http://www.w3.org/1999/xhtml">

        <title>AutoComplete Box with jQuery</title>
        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <style>
            .ui-autocomplete {
                max-height: 100px;
                overflow-y: auto;
                /* prevent horizontal scrollbar */
                overflow-x: hidden;
                /* add padding to account for vertical scrollbar */
                padding-right: 20px;
            }
                /* IE 6 doesn't support max-height
                * we use height instead, but this forces the menu to always be this tall
                */
            * html .ui-autocomplete {
                height: 100px;
            }
        </style>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>
        <script type="text/javascript">
            $.extend($.ui.autocomplete.prototype, {
                _renderMenu: function (ul, items) {
                    //remove scroll event to prevent attaching multiple scroll events to one container element
                    $(ul).unbind("scroll");

                    var self = this;
                    self._scrollMenu(ul, items);
                },

                _scrollMenu: function (ul, items) {
                    var self = this;
                    var maxShow =<%=PageSize%> ;
                    var results = [];
                    var pages = Math.ceil(items.length / maxShow);
                    results = items.slice(0, maxShow);

                   // if (pages > 1) {
                        $(ul).scroll(function () {
                            if (isScrollbarBottom($(ul))) {
                                ++window.pageIndex;
                                if (window.pageIndex >= pages) return;

                                results = items.slice(window.pageIndex * maxShow, window.pageIndex * maxShow + maxShow);

                                //append item to ul
                                $.each(results, function (index, item) {
                                    self._renderItem(ul, item);
                                });
                                //refresh menu
                                self.menu.deactivate();
                                self.menu.refresh();
                                // size and position menu
                                ul.show();
                                self._resizeMenu();
                                ul.position($.extend({
                                    of: self.element
                                }, self.options.position));
                                if (self.options.autoFocus) {
                                    self.menu.next(new $.Event("mouseover"));
                                }
                            }
                        });
                   // }

                    $.each(results, function (index, item) {
                        self._renderItem(ul, item);
                    });
                }
            });

            function isScrollbarBottom(container) {
                var height = container.outerHeight();
                var scrollHeight = container[0].scrollHeight;
                var scrollTop = container.scrollTop();
                if (scrollTop >= scrollHeight - height) {
                    return true;
                }
                return false;
            };

            $(function () {
                $(".tb").autocomplete({
                    source: function (request, response) {                        
                        $.ajax({
                            url: "WebService1.asmx/GetData",
                            data: "{ 'suppRef': '" + request.term + "','pageNumber': '" + 1 + "','pageSize': '" + <%=PageSize%> + "' }",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            dataFilter: function (data) { return data; },
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.SupplierReference,
                                        value: item.SupplierID
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    minLength: 2
                }).focus(function () {
                    //reset result list's pageindex when focus on
                    window.pageIndex = 0;
                    $(this).autocomplete("search");
                });
            });
        </script>
        <body>
            <div class="demo">
                <div class="ui-widget">
                    <label for="tbAuto">Enter Email: </label>
                    <asp:TextBox ID="tbAuto" class="tb" runat="server">
                    </asp:TextBox>
                </div>
            </div>
        </body>
        </html>
    </div>
</asp:Content>
